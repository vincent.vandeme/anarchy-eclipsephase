import { MODULE_NAME, STYLE_PATH } from './constants.js';
import { ECLIPSE_PHASE_SKILLS } from './skills.js';

const ECLIPSEPHASE_CHECKBARS_HACK = {
  plot: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/danger-point-off.webp`, 'checkbar-img')
  },
  anarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  },
  sceneAnarchy: {
    iconChecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-scene.webp`, 'checkbar-img'),
    iconUnchecked: ANARCHY_ICONS.iconPath(`${STYLE_PATH}/anarchy-point-off.webp`, 'checkbar-img')
  }
}

const ECLIPSEPHASE_ANARCHY_HACK = {
  id: MODULE_NAME,
  name: 'Eclipse Phase Anarchy hack',
  hack: {
    checkbars: () => ECLIPSEPHASE_CHECKBARS_HACK
  }
}

export class EclipsePhaseModule {
  static start() {
    const eclipsephaseModule = new EclipsePhaseModule();
    Hooks.once('init', async () => await eclipsephaseModule.onInit());
  }

  async onInit() {
    game.modules.eclipsephaseModule = this;
    Hooks.on(ANARCHY_HOOKS.ANARCHY_HACK, register => register(ECLIPSEPHASE_ANARCHY_HACK));
    Hooks.on(ANARCHY_HOOKS.PROVIDE_SKILL_SET, provide => {
      provide('eclipsephase-anarchy', 'Eclipse Phase Anarchy Hack - FR', ECLIPSE_PHASE_SKILLS.map(it => {
        it.labelkey = `ECLIPSE_PHASE.skill.${it.code}`;
        return it;
      }));
    });
  }
}