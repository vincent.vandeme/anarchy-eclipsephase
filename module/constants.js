export const MODULE_NAME = 'anarchy-eclipsephase';
export const MODULE_PATH = `modules/${MODULE_NAME}`;
export const STYLE_PATH = `${MODULE_PATH}/styles`;
