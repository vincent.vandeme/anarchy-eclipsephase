const { ANARCHY_SYSTEM, ICONS_SKILLS_PATH, TEMPLATE } = ANARCHY_CONSTANTS;

const ATTR = TEMPLATE.attributes;
const DEFENSE = ANARCHY_SYSTEM.defenses;

export const ECLIPSE_PHASE_SKILLS = [
  { code: 'athletisme', attribute: ATTR.strength, icon: `${ICONS_SKILLS_PATH}/athletics.svg` },
  { code: 'acrobaties', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/escape-artist.svg` },
  { code: 'armesAfeu', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/projectile-weapons.svg`, defense: DEFENSE.physicalDefense },
  { code: 'armesAprojectiles', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/projectile-weapons.svg`, defense: DEFENSE.physicalDefense },
  { code: 'armesDeVehicules', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/vehicle-weapons.svg`, defense: DEFENSE.physicalDefense },
  { code: 'armesLourdes', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/vehicle-weapons.svg`, defense: DEFENSE.physicalDefense },
  { code: 'chutelibre', attribute: ATTR.strength, icon: `${ICONS_SKILLS_PATH}/?.svg` },
  { code: 'corpsACorps', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/close-combat.svg`, defense: DEFENSE.physicalDefense },
  { code: 'furtivite', attribute: ATTR.agility, icon: `${ICONS_SKILLS_PATH}/stealth.svg` },
  { code: 'vehiculeAquatique', attribute: ATTR.agility, /* icon: `${ICONS_SKILLS_PATH}/piloting-other.svg` */ },
  { code: 'vehiculeVolant', attribute: ATTR.agility, /* icon: `${ICONS_SKILLS_PATH}/piloting-other.svg`*/ },
  { code: 'vehiculeTerrestres', attribute: ATTR.agility, /* icon: `${ICONS_SKILLS_PATH}/piloting-ground.svg`*/ },
  { code: 'vol', attribute: ATTR.strength, icon: `${ICONS_SKILLS_PATH}/?.svg` },

  { code: 'combatVirtuel', attribute: ATTR.willpower, icon: `${ICONS_SKILLS_PATH}/astral-combat.svg`, defense: DEFENSE.mentalDefense },
  { code: 'psyPasse', attribute: ATTR.willpower, hasDrain: true, icon: `${ICONS_SKILLS_PATH}/?.svg` },
  { code: 'survie', attribute: ATTR.willpower, icon: `${ICONS_SKILLS_PATH}/survival.svg` },

  { code: 'biotech', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/biotech.svg` },
  { code: 'demolition', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/?.svg` },
  { code: 'electronique', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/?.svg` },
  { code: 'hacking', attribute: ATTR.logic, hasConvergence: true, icon: `${ICONS_SKILLS_PATH}/hacking.svg`, defense: DEFENSE.matrixDefense },
  { code: 'ingenierie', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/engineering.svg` },
  { code: 'pistage', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/tracking.svg` },
  { code: 'psychochirurgie', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/?.svg` },
  { code: 'psychologie', attribute: ATTR.logic, icon: `${ICONS_SKILLS_PATH}/?.svg` },

  { code: 'animaux', attribute: ATTR.charisma, icon: `${ICONS_SKILLS_PATH}/animals.svg` },
  { code: 'art', attribute: ATTR.charisma, isSocial: true, /* icon: `${ICONS_SKILLS_PATH}/con-art.svg`*/ },
  { code: 'comedie', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/con-art.svg` },
  { code: 'etiquette', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/etiquette.svg` },
  { code: 'intimidation', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/intimidation.svg` },
  { code: 'negociation', attribute: ATTR.charisma, isSocial: true, icon: `${ICONS_SKILLS_PATH}/negotiation.svg` },
  { code: 'reseau', attribute: ATTR.charisma, isSocial: true, /*icon: `${ICONS_SKILLS_PATH}/negotiation.svg`*/ },
]